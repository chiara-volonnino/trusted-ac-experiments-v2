variables:
  random: &random
    min: 0
    max: 2
    step: 1
    default: 0
  commRadius: &commRadius
    min: 1.6
    max: 2.7
    step: 0.25
    default: 2.0
  turnIntoFakeAt: &turnIntoFakeAt
    formula: 30
  startTrustAlgorithmAt: &startTrustAlgorithmAt
    min: 0
    max: 32
    step: 30
    default: 0
  minObservations: &minObservations
    formula: 4
  observationWindowSize: &observationWindowSize
    min: 4
    max: 15
    step: 5
    default: 20
  trustThreshold: &trustThreshold
    min: 0.80
    step: 0.05
    max: 0.95
    default: 0.85
  maxErrorFactorExp: &maxErrorFactorExp
    min: 0
    max: 16
    step: 1
    default: 8
  maxErrorFactor: &maxErrorFactor
    formula: "Math.pow(2,maxErrorFactorExp)"
  errorLBfactor: &errorLBfactor
    min: 0.5
    max: 2.5
    step: 0.5
    default: 2.5
  errorLB: &errorLB
    formula: "commRadius * errorLBfactor"
  distanceFromSource: &distanceFromSource
    min: 5
    max: 15
    step: 4
    default: 9
  fakeGridStart: &fakeGridStart
    formula: "15-distanceFromSource"
  fakeGridEnd: &fakeGridEnd
    formula: "15+distanceFromSource"
  fakeGridSize: &fakeGridSize
    formula: "fakeGridEnd-fakeGridStart"
  turnIntoSourceAt: &turnIntoSourceAt
    formula: 0
  fakeAreaSize: &fakeAreaSize
    formula: 0.0
  useTrust: &useTrust
    formula: 1
  useRecomm: &useRecomm
    formula: 1
  fakeSpeed: &fakeSpeed
    min: 0.0
    max: 1.0
    step: 0.05
    default: 1
  fakeSpeedValue:
    #language: scala
    #formula: "Vector(0.1, 0.5, 1.0, 1.5, 2.5, 5.0, 10.0)(fakeSpeed.toInt)"
    formula: "fakeSpeed"
    
          
export:
  - time
  - molecule: error-fakes
    aggregators: [sum]
    value-filter: onlyfinite
  - molecule: error-trust
    aggregators: [sum]
    value-filter: onlyfinite
  - molecule: error-trust-recomm
    aggregators: [sum]
    value-filter: onlyfinite
  - molecule: n-error-fakes
    aggregators: [sum]
  - molecule: n-error-trust
    aggregators: [sum]
  - molecule: n-error-trust-recomm
    aggregators: [sum]
  - molecule: distrusted
    aggregators: [sum]
  - molecule: distrusted-recomm
    aggregators: [sum]

seeds:
  scenario: *random
  simulation: *random

network-model:
  type: ConnectWithinDistance
  parameters: [*commRadius]
  
#incarnation: protelis
incarnation: scafi

pools:
  - pool: &program
    - time-distribution: 1
      type: Event
      actions:
        - type: RunScafiProgram #RunProtelisProgram
          parameters: [gradient, 20]
  - pool: &turnIntoFake
    - time-distribution: 
        type: Trigger
        parameters: [*turnIntoFakeAt]
      type: Event
      actions:
        - type: SetLocalMoleculeConcentration
          parameters: [fake, 1]
  - pool: &turnIntoSource
    - time-distribution: 
        type: Trigger
        parameters: [*turnIntoSourceAt]
      type: Event
      actions:
        - type: SetLocalMoleculeConcentration
          parameters: [source, 1]
  - pool: &move
    - time-distribution: *fakeSpeed
      type: Event
      actions:
        - type: BackAndForthMovement
          parameters: [0, 10.0, 20.0, 0.5, # xstep,xstart,xend,xrand
                      0.5, 2.0, 28.0, 0.0, # ystep,ystart,yend,yrand
                      *random]  #seed
  - pool: &contents
    - molecule: source
      concentration: 0 # false
    - molecule: fake
      concentration: 0 # false
    - molecule: commRadius
      concentration: *commRadius
    - molecule: startTrustAlgorithmAt
      concentration: *startTrustAlgorithmAt
    - molecule: minObservations
      concentration: *minObservations
    - molecule: observationWindowSize
      concentration: *observationWindowSize
    - molecule: trustThreshold
      concentration: *trustThreshold
    - molecule: errorLB
      concentration: *errorLB
    - molecule: maxErrorFactor
      concentration: *maxErrorFactor
    - molecule: fakeAreaSize
      concentration: *fakeAreaSize
    - molecule: useRecomm
      concentration: *useRecomm
    - molecule: useTrust
      concentration: *useTrust

positions:
  type: Continuous2DEuclidean
  
displacements:
  - in:
      type: Grid 
      parameters: [10, 0, 30, 30, 1, 1, 0.2, 0.2]
    programs: 
      - *program
    contents: *contents
  - in:
      type: Point
      parameters: [15, 15]
    programs: 
      - *turnIntoSource
      - *program
    contents: *contents
  - in:
      type: Grid
      parameters: [*fakeGridStart, *fakeGridStart, *fakeGridEnd, *fakeGridEnd, *fakeGridSize, *fakeGridSize, 0, 0]
    programs: 
      - *program
      - *turnIntoFake
      - *move
    contents: *contents