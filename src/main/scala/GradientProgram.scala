import it.unibo.alchemist.model.scafi.ScafiIncarnationForAlchemist.{ScafiAlchemistSupport, _}

import scala.collection.mutable.{Map => MMap}

class gradient extends AggregateProgram with StandardSensors with ScafiAlchemistSupport
  with FieldCalculusSyntax with FieldUtils with CustomSpawn with MyLib {
  lazy val trustThreshold = node.get[Double]("trustThreshold")
  lazy val startTrustAlgorithmAt = node.get[Double]("startTrustAlgorithmAt")
  lazy val observationWindow = node.get[Double]("observationWindowSize").toInt
  lazy val minObservations = node.get[Int]("minObservations")
  lazy val commRadius = node.get[Double]("commRadius")
  lazy val errorLB = node.get[Double]("errorLB")
  lazy val sfunction = node.get[Double]("sfunction").toInt
  lazy val maxErrorFactor = node.get[Double]("maxErrorFactor")
  lazy val fakeAreaSize = node.get[Double]("fakeAreaSize")
  
  lazy val useRecommendations: Boolean = sense[Int]("useRecomm")==1
  lazy val useTrust: Boolean = sense[Int]("useTrust")==1
  lazy val performAnalysis: Boolean = sense[Int]("performAnalysis")==1

  def isSrc: Boolean = sense[Int]("source")==1
  def isFake: Boolean = sense[Int]("fake")==1
  //def isInFakeArea: Boolean = gradient(isFake)<=fakeAreaSize
  lazy val willBeFake: Boolean = sense[Int]("willBeFake")==1

  import FieldUtilsV1.withoutSelf._
  import excludingSelf._

  var fakeValue: Double = 0.0
  val keepFakeValueTime = 0
  def nextFakeValue = nextRandom *30 // 2
  
  // Keeps a pair (a,b) for each node that is used to calculate a trust score
  var map = Map[ID,(Double,Double)]()

  override def main(): Double = {
    val fake = isFake

    branch(fake){ // Sets fake value to be produced in this round
      fakeValue = keepFor(keepFakeValueTime, nextFakeValue)
    }{ /* do nothing */ }

    node.put("merrdiff", 0)

    val dist = gradient(isSrc)
    node.put[Double]("gradient", dist)
    
    val distWithFakes = gradient(isSrc, fake)
    node.put[Double]("gradientUnderAttack", distWithFakes)
    node.put[Double]("error-fakes", Math.abs(dist-distWithFakes))
    node.put[Boolean]("n-error-fakes", Math.abs(dist-distWithFakes) > (5*dist/100))

    branch(useTrust){
    val distWithTrust = gradientWithTrust(isSrc, fake)
    node.put[Double]("gradientWithTrust", distWithTrust)
    node.put[Double]("error-trust", Math.abs(dist-distWithTrust))
    node.put[Boolean]("n-error-trust", Math.abs(dist-distWithTrust) > (5*dist/100))
    }{}

    branch(useRecommendations){
    val distWithTrustRecomm = gradientWithTrustPlusRecommendations(isSrc, fake)
    node.put[Double]("gradientWithTrustPlusRecs", distWithTrustRecomm)
    node.put[Double]("error-trust-recomm", Math.abs(dist-distWithTrustRecomm))
    node.put[Boolean]("n-error-trust-recomm", Math.abs(dist-distWithTrustRecomm) > (5*dist/100))
    }{}
    
    dist
  }

  def gradient(source: Boolean, fake: Boolean = false): Double =
    rep(Double.PositiveInfinity)(
      distance => mux(source) { 0.0 } {
        def shareDistance = if(!fake) nbr(distance) else nbr(fakeValue)
        minHoodPlus(shareDistance + nbrRange)
      }
  )
  
  /**
   * a: represent a positive observation
   * b: represent a negative observation
   */
  case class TrustParams(a: Double, b: Double, numObservations: Int)
  case class TrustProfile(nbrId: ID, params: TrustParams, value: Double)

  def gradientWithTrust(source: Boolean, fake: Boolean = false, startAt: Double = startTrustAlgorithmAt): Double = {
    rep(Double.PositiveInfinity){ distance =>
      val dist = if(!fake) distance else fakeValue
      def nbrDist = nbr { dist }
      val n = countHood(nbrDist.isFinite)
      val sumValues = sumHood(mux(nbrDist.isFinite){ nbrDist }{ 0.0 } )
      val xmean =  sumValues / n
      val sumSqDev = sumHood(mux(nbrDist.isFinite){ Math.pow(dist - xmean, 2) } { 0.0 })
      val s = Math.sqrt(sumSqDev / n)

      branch(performAnalysis){
        val fakeNbr = anyHood{ nbr(willBeFake) }
        val nearFake = anyHood { nbr(fakeNbr) } && !fakeNbr
        val nearNearFake = anyHood { nbr(nearFake) } && !nearFake
        node.put("fake_nbr", fakeNbr)
        node.put("fake_near", nearFake)
        node.put("s_value_nbr_fake", if(fakeNbr) s else Double.NaN)
        node.put("s_value_near_fake", if(nearFake) s else Double.NaN)
        node.put("s_value_near_near_fake", if(nearNearFake) s else Double.NaN)
        node.put("s_value_far_fake", if(!nearNearFake && !nearFake && !fakeNbr) s else Double.NaN)
      }{}

      mux(source) { 0.0 } {
        branch(currTime > startAt){
          var distrustedNbrs = List[ID]()
          val res = foldhoodPlus(Double.PositiveInfinity)(Math.min){
            val trustParams = calculateTrustParams(dist, xmean, s)
            val trustValue = beta(trustParams.a, trustParams.b)
            val isTrusted = if(trustParams.numObservations >= minObservations) trustable(trustValue) else true
            val nbrId = nbr{mid()}
            if(!isTrusted) { distrustedNbrs = nbrId :: distrustedNbrs}
            val newg = mux(isTrusted){ nbr{dist} + nbrRange }{ Double.PositiveInfinity } // dist/Math.pow(trustValue,4)
            newg
          }
          node.put("distrustedNbrs", distrustedNbrs)
          val distrustedBy = foldhoodPlus(Map[ID,List[ID]]())(_++_)(Map(nbr{mid()->distrustedNbrs})).filter(_._2.contains(mid())).keySet

          val distrusted = distrustedBy.nonEmpty
          node.put("distrusted", distrusted)
          node.put("distrustedBy", distrustedBy)
          res.orIf(_==Double.PositiveInfinity){dist}
        }{
          minHood( nbr{dist} + nbrRange )
        }
      }
    }
  }

  def gradientWithTrustPlusRecommendations(source: Boolean, fake: Boolean = false, startAt: Double = startTrustAlgorithmAt): Double = {
    rep(Double.PositiveInfinity){ distance =>
      val dist = if(!fake) distance else fakeValue
      def nbrDist = nbr { dist }
      val n = countHood(nbrDist.isFinite)
      val sumValues = sumHood(mux(nbrDist.isFinite){ nbrDist }{ 0.0 } )
      val xmean =  sumValues / n
      val sumSqDev = sumHood(mux(nbrDist.isFinite){ Math.pow(dist - xmean, 2) } { 0.0 })
      val s = Math.sqrt(sumSqDev / n)

      mux(source) { 0.0 } {
        branch(currTime > startAt){
          var distrustedNbrs = List[ID]()
          // NOTE: beware of foldhoodPlus with side effects:
          // as it is impl by now, on top of foldhood,
          // the expression is run for the 'self' as well!
          val localTrustProfiles = foldhoodPlus(Map[ID,TrustProfile]())(_++_){
            val nbrId = nbr{mid()}
            val trustParams = calculateTrustParams(dist, xmean, s)
            Map(nbrId -> TrustProfile(nbrId, trustParams, nbr{dist}+nbrRange))
          }

          val nbrTrustProfiles = foldhoodPlus(Map[ID,Map[ID,TrustProfile]]())(_++_){
            Map(nbr{mid -> localTrustProfiles})
          }

          // NOTE: beware using aggregate constructs inside foldhood:
          // results of inner nodes are reused by all the neighbours!
          // Also: remember that foldhood.aggregate.nbr is to be ignored
          val res = foldhoodPlus(Double.PositiveInfinity)(Math.min){
            mux(!nbrDist.isFinite){
              node.put(s"trustParams_${nbr(mid())}", s"NOT FINITE: $nbrDist")
              Double.PositiveInfinity
            }{
              val nbrId = nbr{mid()}
              val (aRec: Double, bRec: Double) = nbrTrustProfiles
                .mapValues(_.get(nbrId).map(p => (p.params.a, p.params.b)).getOrElse(0.0,0.0))
                .foldLeft((0.0, 0.0))((acc, value) => {
                  // i = mid, j = nbrId ; a_j and b_j calculated from all nbrs k != i,j
                  val TrustParams(a_ik, b_ik, _) = localTrustProfiles.get(value._1).map(_.params).getOrElse(TrustParams(0.0,0.0,0))
                  val a_kj = value._2._1
                  val b_kj = value._2._2
                  val denom = (b_ik+2)*(a_kj+b_kj+2)+2*a_ik
                  val a_j = acc._1 + 2*a_ik*a_kj/denom
                  val b_j = acc._2 + 2*a_ik*b_kj/denom
                  (a_j, b_j)
                })
              val localParams = localTrustProfiles.get(nbrId).map(_.params).getOrElse(TrustParams(0.0,0.0,0))
              val a = localParams.a + aRec
              val b = localParams.b + bRec
              val trustValue = beta(a,b)
              val isTrusted = if(localParams.numObservations >= minObservations) trustable(trustValue) else true
              if(!isTrusted) { distrustedNbrs = nbrId :: distrustedNbrs}
              val newg = mux(isTrusted){ nbr{dist} + nbrRange }{ Double.PositiveInfinity }
              newg
            }
          }

          node.put("distrustedNbrs", distrustedNbrs)
          val distrustedBy = foldhoodPlus(Map[ID,List[ID]]())(_++_)(Map(nbr{mid()->distrustedNbrs})).filter(_._2.contains(mid())).keySet

          val distrusted = distrustedBy.nonEmpty
          node.put("distrusted-recomm", distrusted)
          node.put("distrustedBy-recomm", distrustedBy)
          res.orIf(_==Double.PositiveInfinity){dist}
        }{
          minHood( nbr{dist} + nbrRange )
        }
      }
    }
  }

  /**
   * Returns a score based on a beta distribution.
   */
  def calculateTrustParams(field: => Double, xmean: Double, s: Double): TrustParams = {
    val (nbrId, nbrVal) = nbr{ (mid(), field) }
    val deviation = Math.abs(nbrVal - xmean)
    val maxError = if(sfunction == 0){
      errorLB
    } else {
      Math.max(s, errorLB)
    }
    if(maxError > (errorLB+0.1)) node.put("merrdiff", 1.0)

    type MutableField[T] = MMap[ID,T]
    def MutableField[T](): MutableField[T] = MMap[ID,T]()
    type AlfaBetaPair = (Double, Double)
    type AlfaBetaHistory = List[AlfaBetaPair]

    val m = rep(MutableField[AlfaBetaHistory]()){ m => m }

    val history = m.getOrElse(nbrId, List())
    //env.put(s"trust_LASTcheck_${nbrId}__", s"|$nbrVal - $xmean| = $deviation > ${maxError} ($s*$maxErrorFactor) ===> ${deviation > maxError}")
    val obsEval = if(nbrVal.isFinite && s.isFinite){
      if(deviation > maxError) (0.0, 1.0) else (1.0, 0.0)
    } else {
      (0.0,0.0)
    }
    if(obsEval._1!=0 || obsEval._2!=0)
      m.put(nbrId, (obsEval :: history).take(observationWindow))

    val obss = m.getOrElse(nbrId, List())

    val (a,b) = obss.foldRight((0.0,0.0))((t,u) => (t._1+u._1, t._2+u._2))
    //env.put(s"trust_params(a,b)_${nbr(mid())}_", (a,b,obss.size))
    TrustParams(a, b, obss.size)
  }

  def beta(a: Double, b: Double) = (a+1)/(a+b+2)

  def trustable(trustValue: Double): Boolean = trustValue >= trustThreshold
}


trait MyLib { self: AggregateProgram with StandardSensors with FieldUtils with ScafiAlchemistSupport =>
  //def nbrRange = nbrvar[Double](NBR_RANGE)
  //def currTime: Double = sense[Time]("time").toDouble
  def currTime: Double = timestamp() // todo: there is a currenTime() in Incarnation trait. But I don't know difference
  //def deltaTime: Double = sense[Double]("dt")
  //def deltaTime: Double = alchemistDeltaTime()
  //def nextRandom: Double = sense[()=>java.lang.Double]("random")().toDouble

  case class FoldingOptions(onlyFinite: Boolean = true)

  def keepFor(time: Double, f: => Double): Double = rep[(Double,Double)]((currTime,f)){ case (lastTime,oldVal) =>
    mux(currTime-lastTime > time){ (currTime, f) } { (lastTime, oldVal) }
  }._2

  def meanCounter(value: Double, frequency: Long): Double = {
    val time = currTime
    val dt = sense[Double]("dt") // or deltaTime() todo: ???
    val count = rep ((0.0,0.0)) { x => { // (accumulated value, last time)
      // Splits into windows of multiples of 'frequency'
      // and restarts at the beginning of each new window.
      // E.g., for frequency=5
      // Time:           0_____5_____10_____15_____20 ...
      // Restart:              ^      ^      ^      ^ ...
      // Floor(Time/freq):  0     1      2      3     ...
      val restart = rep((false, time)) { t =>
        (Math.floor(time / frequency) > Math.floor(t._2 / frequency), time)
      }._1
      // Reset value and time on restart
      val old = if (restart) (0.0, 0.0) else x
      // Filters infinite values out
      if (Double.NegativeInfinity < value && value < Double.PositiveInfinity) {
        // Sums value weighed by time
        (old._1 + value * dt, old._2 + dt)
      } else old
    }
    }
    // E.g., consider these values and deltas: (5.0,2), (6,1), (Inf,2), (7,1), (5,1)
    // You'll finally have (5.0*2 + 6*1 + 7*1 + 5*1) / (2+1+1+1) = 28/5 = 5.6
    count._1 / count._2
  }

  implicit class RichValue[T](val value: T){
    def orIf(pred: T => Boolean)(newValue: T): T = orIfCompute(pred)(_ => newValue)
    def orIfCompute(pred: T => Boolean)(compute: T => T): T = if(pred(value)) compute(value) else value
  }

  implicit class RichPred[T](val p: T => Boolean) {
    def not: T => Boolean = v => !p(v)
  }

  implicit class MyRichDouble(val d: Double) {
    def isFinite: Boolean = !d.isInfinite && !d.isNaN
  }

  trait FieldUtilsV1 {
    val fops = FoldingOptions()

    def minHood(field: => Double) =
      fold[Double](Double.PositiveInfinity)(Math.min)(field)
    def countHood(field: => Boolean)(implicit fops: FoldingOptions = FoldingOptions()) =
      fold[Int](0)(_+_)(if(field) 1 else 0)
    def fold[T]: T => ((T, T) => T) => ( => T ) => T
  }

  object FieldUtilsV1 {
    val withSelf = new FieldUtilsV1 { override def fold[T]: T => ((T, T) => T) => ( => T ) => T = foldhood[T](_) }
    val withoutSelf = new FieldUtilsV1 { override def fold[T]: T => ((T, T) => T) => ( => T ) => T = foldhoodPlus[T](_) }
  }
}